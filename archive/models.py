from uuid import uuid4

from django.db import models
from django.utils.functional import cached_property
from django.utils.translation import gettext as _t

from archive.consts import VideoFormats
from archive.tasks import make_preview, prepare_video


class PUided(models.Model):
    """
    Primary Uided
    """
    UID_PREFIX = 'OBJ'
    uid = models.UUIDField(primary_key=True, default=uuid4, editable=False)

    @cached_property
    def suid(self) -> str:
        """
        String representation of UID
        """
        return "{{{0}-{1}}}".format(self.UID_PREFIX, self.uid).lower()

    def __str__(self):
        return self.suid

    class Meta:
        abstract = True


class Unit(PUided):
    title = models.CharField(verbose_name=_t('Title'), max_length=256)
    raw_file = models.FileField(verbose_name=_t('Raw file'), null=True)
    mp4_file = models.FileField(
        verbose_name=_t('Compressed mp4 file'),
        null=True)
    webm_file = models.FileField(
        verbose_name=_t('Compressed webm file'),
        null=True)
    preview = models.ImageField(
        verbose_name=_t('File preview'),
        max_length=256,
        null=True)
    description = models.TextField(
        verbose_name=_t('File description'),
        null=True,
        blank=True)
    error_description = models.TextField(
        verbose_name=_t('Error description'),
        null=True,
        blank=True)
    created = models.DateTimeField(
        editable=False, auto_now_add=True, verbose_name=_t('Created'),
        db_index=True
    )

    def save(self, proceed_file=False,  # type: ignore
             force_insert=False, force_update=False, using=None,
             update_fields=None) -> None:
        super().save(force_insert, force_update, using, update_fields)
        if self.raw_file and proceed_file:
            make_preview.apply_async((self.uid,))
            prepare_video.apply_async((self.uid, VideoFormats.MP4))
            prepare_video.apply_async((self.uid, VideoFormats.WEBM))

    class Meta:
        ordering = ['-created']
