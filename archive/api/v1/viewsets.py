from rest_framework import mixins, viewsets

from archive.api.v1.serializers import UnitSerializer
from archive.models import Unit


class UnitViewSet(mixins.ListModelMixin,
                  mixins.CreateModelMixin,
                  mixins.DestroyModelMixin,
                  viewsets.GenericViewSet):
    serializer_class = UnitSerializer

    def get_queryset(self):
        return Unit.objects.all()
