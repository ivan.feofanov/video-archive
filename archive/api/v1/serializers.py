from django.conf import settings
from django.utils.translation import gettext as _t

from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.request import Request

from archive.models import Unit
from archive.tasks import download_file


class UnitSerializer(serializers.ModelSerializer):
    preview = serializers.SerializerMethodField()
    file_link = serializers.URLField(write_only=True, required=False)

    def validate_raw_file(self, raw_file):
        file_link = self.initial_data.get('file_link')
        if not raw_file and not file_link:
            raise ValidationError(_t('raw_field or file_link required'))
        if raw_file and file_link:
            raise ValidationError(_t('Ambiguous information: choose '
                                     'raw_field or file_link'))
        return raw_file

    def get_preview(self, obj: Unit) -> str:
        request: Request = self.context.get('request')  # type: ignore
        url = obj.preview.url if obj.preview else settings.DEFAULT_PREVIEW_URL
        return request.build_absolute_uri(url)

    @staticmethod
    def create(validated_data):
        file_link = validated_data.pop('file_link', None)
        unit: Unit = Unit(**validated_data)
        unit.save(proceed_file=True)
        if file_link:
            download_file.apply_async((file_link, unit.uid))
        return unit

    class Meta:
        model = Unit
        fields = (
            'uid',
            'title',
            'description',
            'created',
            'mp4_file',
            'webm_file',
            'raw_file',
            'preview',
            'file_link'
        )
