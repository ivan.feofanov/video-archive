from datetime import datetime

from freezegun import freeze_time

from archive.models import Unit
from archive.tests.factories import UnitFactory


@freeze_time(datetime.now())
def test_created():
    now = datetime.now().astimezone()
    unit = UnitFactory()

    assert unit.created == now


def test_ordering():
    with freeze_time('2020-01-01'):
        unit0 = UnitFactory()
    with freeze_time('1900-01-01'):
        unit1 = UnitFactory()

    units = Unit.objects.all()
    assert units[0] == unit0
    assert units[1] == unit1
