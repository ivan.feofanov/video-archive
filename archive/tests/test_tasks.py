from unittest.mock import patch

import ffmpeg
import pytest
import responses
from PIL import Image
from django.core.files.base import ContentFile

from archive.consts import VideoFormats
from archive.models import Unit
from archive.tests.factories import UnitFactory
from archive.tasks import make_preview, download_file, prepare_video


@patch('archive.models.make_preview.apply_async')
@patch('archive.models.prepare_video.apply_async')
def test_create_preview(mock_0, mock_1, settings, faker, avi_file):
    unit: Unit = UnitFactory()
    with open(avi_file, 'rb') as test_file:
        unit.raw_file.save(f'test.avi', ContentFile(test_file.read()))

    make_preview(unit_id=unit.uid)

    unit.refresh_from_db()

    preview = Image.open(unit.preview)

    assert preview.height == settings.VIDEO_HEIGHT
    assert preview.width == settings.VIDEO_WIDTH
    assert unit.preview.name == f'{unit.uid}-preview.jpg'


@pytest.mark.parametrize('video_format', ('mp4', 'webm'))
@patch('archive.models.make_preview.apply_async')
@patch('archive.models.prepare_video.apply_async')
def test_prepare_video(
        mock_0, mock_1, settings, faker, avi_file, video_format):

    unit: Unit = UnitFactory()
    with open(avi_file, 'rb') as test_file:
        unit.raw_file.save(f'test.avi', ContentFile(test_file.read()))

        prepare_video(unit_id=unit.uid, video_format=video_format)

    unit.refresh_from_db()

    if video_format == VideoFormats.MP4:
        assert unit.mp4_file
        video = ffmpeg.probe(unit.mp4_file.path)
    elif video_format == VideoFormats.WEBM:
        assert unit.webm_file
        video = ffmpeg.probe(unit.webm_file.path)
    else:
        video = None

    video_stream = next((stream for stream in video['streams']
                         if stream['codec_type'] == 'video'), None)
    assert video_stream['height'] == settings.VIDEO_HEIGHT
    assert video_stream['width'] == settings.VIDEO_WIDTH


@patch('archive.models.make_preview.apply_async')
@patch('archive.models.prepare_video.apply_async')
def test_prepare_video_error(mock_0, mock_1, settings, faker, avi_file):
    unit: Unit = UnitFactory()
    with open(avi_file, 'rb') as test_file:
        unit.raw_file.save(f'test.avi', ContentFile(b'wrong content'))

        prepare_video(unit_id=unit.uid)

    unit.refresh_from_db()

    assert unit.error_description is not None


@responses.activate
@patch('archive.models.make_preview.apply_async')
@patch('archive.models.prepare_video.apply_async')
def test_download_file(mock_0, mock_1, faker, avi_file):
    unit: Unit = UnitFactory()
    file_link = faker.uri()
    with open(avi_file, 'rb') as test_file:
        cd_header = {
            'Content-Disposition': f"attachment; filename*=UTF-8''test.avi"}
        responses.mock.add(responses.GET,
                           file_link,
                           body=test_file.read(),
                           headers=cd_header)
        download_file(uri=file_link, unit_id=unit.uid)

    unit.refresh_from_db()

    assert unit.raw_file
    assert unit.raw_file.name == 'test.avi'
