import factory.fuzzy
from django.core.files.base import ContentFile

from archive.models import Unit


class UnitFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Unit

    preview = factory.LazyAttribute(
        lambda _: ContentFile(
            factory.django.ImageField()._make_data(
                {'width': 1024, 'height': 768}
            ), 'example.jpg'
        )
    )
