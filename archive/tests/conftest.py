import os

import pytest
from django.conf import settings
from django.urls import reverse


@pytest.fixture(scope='module')
def avi_file() -> str:
    dir_name = os.path.dirname(__file__)
    return os.path.join(settings.BASE_DIR, dir_name, 'test.avi')


@pytest.fixture
def api_url():
    return reverse('unit-list')
