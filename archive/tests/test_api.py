from datetime import datetime
from unittest.mock import patch

import pytest
import responses
from freezegun import freeze_time
from rest_framework import status

from _project_.tests.factories import UserFactory
from archive.consts import VideoFormats
from archive.models import Unit
from archive.tests.factories import UnitFactory


def test_auth(client, api_url):
    res = client.get(api_url)
    assert res.status_code == status.HTTP_401_UNAUTHORIZED

    user = UserFactory.create()
    client.force_login(user)

    res = client.get(api_url)
    assert res.status_code == status.HTTP_200_OK


@freeze_time()
@responses.activate
@patch('archive.models.prepare_video.apply_async')
@patch('archive.models.make_preview.apply_async')
@pytest.mark.parametrize('has_description', (True, False))
@pytest.mark.parametrize('request_type', ('link', 'file'))
def test_post_file(
        mock_preview, mock_prepare_video, faker, logged_user_client,
        api_url, avi_file, has_description, request_type):
    title = faker.pystr()
    file_link = faker.uri()
    description = faker.pystr() if has_description is True else None
    data = {
        'title': title,
    }
    if has_description is True:
        data['description'] = description

    with open(avi_file, 'rb') as test_file:
        if request_type == 'link':
            cd_header = {'Content-Disposition': f"attachment; "
                                                f"filename*=UTF-8''test.avi"}
            responses.mock.add(responses.GET,
                               file_link,
                               body=test_file.read(),
                               headers=cd_header)
            data['file_link'] = file_link
        else:
            data['raw_file'] = test_file

        res = logged_user_client.post(api_url, data=data)

    assert res.status_code == status.HTTP_201_CREATED

    unit = Unit.objects.first()

    assert res.data['uid'] == str(unit.uid)
    assert res.data['title'] == title
    assert res.data['description'] == description
    assert res.data['created'] == f'{datetime.now().isoformat()}Z'  # python datetime doesn't include timezone information
    assert res.data['preview'] == 'http://testserver/static/images/preview.jpg'

    mock_preview.assert_called_with((unit.uid,))
    mock_prepare_video.assert_any_call((unit.uid, VideoFormats.MP4))
    mock_prepare_video.assert_any_call((unit.uid, VideoFormats.WEBM))


def test_post_file_without_title(faker, logged_user_client, avi_file, api_url):
    with open(avi_file, 'rb') as test_file:
        res = logged_user_client.post(api_url, data={
            'raw_file': test_file
        })

    assert res.status_code == status.HTTP_400_BAD_REQUEST


def test_post_not_video_file(faker, logged_user_client, api_url):
    res = logged_user_client.post(api_url, data={
        'title': faker.pystr(),
        'raw_file': b'wrong content'
    })

    assert res.status_code == status.HTTP_400_BAD_REQUEST


@patch('archive.models.make_preview.apply_async')
@pytest.mark.parametrize('has_preview, preview_url', (
        (True, 'http://testserver/media/example.jpg'),
        (False, 'http://testserver/static/images/preview.jpg')
))
def test_preview(mock, logged_user_client, api_url, has_preview, preview_url):
    if has_preview:
        UnitFactory()
    else:
        UnitFactory(preview=None)

    res = logged_user_client.get(api_url)
    assert res.data[0]['preview'] == preview_url


def test_delete(logged_user_client, api_url):
    unit = UnitFactory()
    assert Unit.objects.count() == 1

    res = logged_user_client.delete(f'{api_url}{unit.uid}/')
    assert res.status_code == status.HTTP_204_NO_CONTENT
    assert Unit.objects.count() == 0
