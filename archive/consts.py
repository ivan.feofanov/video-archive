from django.db import models
from django.utils.translation import gettext as _t


class VideoFormats(models.TextChoices):
    MP4 = 'mp4', _t('Mpeg4 encoded')
    WEBM = 'webm', _t('Webm encoded')
