from django.contrib import admin

from archive.models import Unit


@admin.register(Unit)
class UnitAdmin(admin.ModelAdmin):
    list_display = ('uid', 'title', 'description')
    search_fields = ('uid', 'title', 'description')
    fieldsets = (
        (
            None,
            {'fields': (
                'title', 'description', 'mp4_file', 'webm_file', 'raw_file'
            )}
        ),
    )
