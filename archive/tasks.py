import subprocess
from typing import Union
from urllib.parse import unquote
from uuid import UUID

import ffmpeg
import requests
from celery import app

from django.utils.translation import gettext as _t
from django.apps import apps
from django.conf import settings
from django.core.files.base import ContentFile

from archive.consts import VideoFormats


@app.shared_task()
def make_preview(unit_id: Union[UUID, str]) -> None:
    Unit = apps.get_model('archive.Unit')  # noqa: pylint=invalid-name
    unit = Unit.objects.get(pk=unit_id)
    out, _ = (
        ffmpeg
        .input(unit.raw_file.path)
        .filter('scale', settings.VIDEO_WIDTH, settings.VIDEO_HEIGHT)
        .filter('select', 'gte(n,{})'.format(settings.PREVIEW_FRAME_NUMBER))
        .output('pipe:', vframes=1, format='image2', vcodec='mjpeg')
        .run(capture_stdout=True)
    )
    file_name = f'{unit_id}-preview.jpg'
    unit.preview.save(file_name, ContentFile(out))


@app.shared_task()
def download_file(uri: str, unit_id: Union[UUID, str]) -> None:
    Unit = apps.get_model('archive.Unit')  # noqa: pylint=invalid-name
    unit = Unit.objects.get(pk=unit_id)
    res = requests.get(uri)
    try:
        res.raise_for_status()
        # Honestly, IDK if content_disposition can be None or not
        file_name = 'noname'
        content_disposition = res.headers.get('content-disposition')
        if content_disposition is not None:
            file_name = unquote(content_disposition.split("-8''")[1])
        unit.raw_file.save(file_name, ContentFile(res.content), save=False)
        unit.save(proceed_file=True)
    except requests.RequestException:
        unit.error_description = res.text
        unit.save()


@app.shared_task()
def prepare_video(
        unit_id: Union[UUID, str], video_format: str = 'mp4') -> None:

    Unit = apps.get_model('archive.Unit')  # noqa: pylint=invalid-name
    unit = Unit.objects.get(pk=unit_id)
    processed_video_file = f'{unit.raw_file.path}.{video_format}'
    processed_video_file_name = f'{unit.raw_file.name}.{video_format}'
    try:
        subprocess.run(['ffmpeg', '-i', unit.raw_file.path,
                        '-f', video_format,
                        '-vf', f'scale={settings.VIDEO_WIDTH}:'
                               f'{settings.VIDEO_HEIGHT}',
                        processed_video_file],
                       stdout=subprocess.PIPE,
                       stderr=subprocess.PIPE,
                       check=True)
        with open(processed_video_file, 'rb') as file:
            if video_format == VideoFormats.MP4:
                unit.mp4_file.save(
                    processed_video_file_name, ContentFile(file.read()))
            elif video_format == VideoFormats.WEBM:
                unit.webm_file.save(
                    processed_video_file_name, ContentFile(file.read()))
            else:
                raise Exception(_t('Unknown video format'))

    except (OSError, subprocess.CalledProcessError) as exc:
        unit.error_description = str(exc)
        unit.save()
