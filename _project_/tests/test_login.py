from django.contrib.auth.models import User
from rest_framework import status

from _project_.tests.factories import UserFactory


def test_success_login(faker, client):
    username = faker.pystr()
    password = faker.pystr()
    user: User = UserFactory.create(username=username)
    user.set_password(password)
    user.save()

    res = client.post('/login/', {
        'username': username,
        'password': password
    })

    assert res.status_code == status.HTTP_200_OK
    assert res.data['token'] is not None


def test_login_error(faker, client):
    username = faker.pystr()
    password = faker.pystr()
    UserFactory.create(username=username)

    res = client.post('/login/', {
        'username': username,
        'password': password
    })

    assert res.status_code == status.HTTP_404_NOT_FOUND
