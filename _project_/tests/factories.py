import factory.fuzzy
from django.contrib.auth.models import User


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User
    username = factory.fuzzy.FuzzyText(length=8)

    is_active = True

