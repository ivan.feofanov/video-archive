import os

from celery.schedules import crontab
from datetime import timedelta

from . import settings

result_backend = settings.REDIS_URL
result_serializer = 'json'
result_expires = 3600

broker_url = settings.REDIS_URL
timezone = settings.TIME_ZONE
task_serializer = 'json'
task_time_limit = 120  # 2 minutes

# task_soft_time_limit требует поддержки в задачах
accept_content = ['json']  # Ignore other content
celery_imports = [
    'core.tasks',
]
worker_max_tasks_per_child = 50000

beat_schedule_filename = os.path.join(settings.DATA_DIR, 'celerybeat.db')
beat_schedule = {
    # Scheduler
    'scheduler_execute': {
        'task': 'scheduler.tasks.execute',
        'schedule': 15.0
    },
}
