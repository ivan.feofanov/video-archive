from django.contrib.auth import authenticate
from rest_framework import views, status
from rest_framework.authtoken.models import Token
from rest_framework.response import Response


class LoginView(views.APIView):
    permission_classes = ()

    @staticmethod
    def post(request):
        data = request.data

        username = data.get('username', None)
        password = data.get('password', None)

        user = authenticate(username=username, password=password)

        if user is not None:
            if user.is_active:
                token, _ = Token.objects.get_or_create(user=user)
                return Response({'token': token.key})
            return Response(status=status.HTTP_404_NOT_FOUND)
        return Response(status=status.HTTP_404_NOT_FOUND)
