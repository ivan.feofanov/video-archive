from rest_framework import routers

from archive.api.v1.viewsets import UnitViewSet

router = routers.DefaultRouter()
router.register('unit-list', UnitViewSet, basename='unit')
