import os
import tempfile

import django
import pytest

from faker import Faker

from _project_.celery import app as celery_app


def pytest_configure():
    os.environ.setdefault("ENVIRONMENT", "test")
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "_project_.settings")
    celery_app.conf.task_always_eager = True
    celery_app.conf.task_eager_propagates = True
    django.setup()


@pytest.fixture(autouse=True)
def enable_db_access(db):
    pass


@pytest.fixture
def client():
    from rest_framework.test import APIClient
    return APIClient()


@pytest.fixture
def logged_user_client(client):
    from _project_.tests.factories import UserFactory

    user = UserFactory.create()
    client.force_login(user)
    client.user = user
    return client


@pytest.fixture
def faker():
    return Faker()


@pytest.fixture(autouse=True)
def use_tmp_media_root(settings):
    settings.MEDIA_ROOT = tempfile.mkdtemp()
