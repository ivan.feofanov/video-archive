#!/bin/sh

python manage.py migrate
echo "Migrated DB to latest version"

UWSGI_PROC_COUNT="${UWSGI_PROC_COUNT:-$(($(nproc) * 2))}"
PORT="${PORT:-5000}"
UWSGI_EXTRA_ARGS="--socket-timeout 180 --http-timeout 180 --listen 128 --max-requests 10000"

#python manage.py migrate
exec uwsgi --static-map /static=$STATIC_ROOT --static-map /media=$MEDIA_ROOT \
  --module _project_.wsgi --processes $UWSGI_PROC_COUNT --http :$PORT \
  --master --die-on-term --enable-threads --single-interpreter \
  --limit-post 4294967296 $UWSGI_EXTRA_ARGS
