#!/bin/bash
CELERY_PROC_COUNT="${CELERY_PROC_COUNT:-2}"
CELERY_PROC_COUNT_WORKER="${CELERY_PROC_COUNT_WORKER:-$CELERY_PROC_COUNT}"

exec python -u -m celery -A _project_ worker\
    --loglevel info \
    --concurrency "$CELERY_PROC_COUNT_WORKER" \
    -n main@%h
